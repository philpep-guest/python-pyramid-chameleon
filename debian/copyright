Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pyramid_chameleon
Source: https://github.com/Pylons/pyramid_chameleon/

Files: *
Copyright: Chris McDonough, 2010/11/08
           Tres Seaver, 2010/11/09
           Blaise Laflamme, 2010/11/14
           Reed O'Brien, 2012/03/12
           Bert JW Regeer, 2013-09-06
           Charlie Clark, 2016-02-01
           Jeremy Davis, 2016-02-01
           Steve Piercy, 2016-02-02
Comment: data from CONTRIBUTORS.txt
License: Pyramid

Files: debian/*
Copyright: 2018 Nicolas Dandrimont <olasd@debian.org>
License: Expat

License: Pyramid
   A copyright notice accompanies this license document that identifies
   the copyright holders.
 .
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
 .
   1.  Redistributions in source code must retain the accompanying
       copyright notice, this list of conditions, and the following
       disclaimer.
 .
   2.  Redistributions in binary form must reproduce the accompanying
       copyright notice, this list of conditions, and the following
       disclaimer in the documentation and/or other materials provided
       with the distribution.
 .
   3.  Names of the copyright holders must not be used to endorse or
       promote products derived from this software without prior
       written permission from the copyright holders.
 .
   4.  If any files are modified, you must cause the modified files to
       carry prominent notices stating that you changed the files and
       the date of any change.
 .
   Disclaimer
 .
     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND
     ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
     TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
     PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
     HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
     TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
     DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
     TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
     THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
     SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
